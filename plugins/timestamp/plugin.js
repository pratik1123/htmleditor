CKEDITOR.plugins.add( 'timestamp', {

	icons: 'timestamp',

	init: function( editor ) {

		editor.addCommand( 'insertTimestamp', {

			exec: function( editor ) {
				let now = new Date()
				let name = prompt("Please Enter Your name")

				if(name){
					editor.insertHtml("<p><br>Name: <b>"+name+"</b>. Current Time is "+ now.toISOString() +"</p>")
				}
			}
		});

		editor.ui.addButton( 'Timestamp', {
			label: 'Insert Timestamp',
			command: 'insertTimestamp',
			toolbar: 'insert'
		});
	}
});
