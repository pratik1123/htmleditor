﻿CKEDITOR.dialog.add( 'myDialog', function(b) {
	function e(a) {
		a = a.element;
		var b = this.getValue();
		b ? a.setAttribute(this.id, b) : a.removeAttribute(this.id);
	}
	function f(a) {
		a = a.hasAttribute(this.id) && a.getAttribute(this.id);
		this.setValue(a || "");
	}
	var g = { email: 1, password: 1, search: 1, tel: 1, text: 1, url: 1 };
	var types = [
		["Text", "text" ],
		["Email", "email"],
		["Password", "password"],
		["Url", "url"],
		["Date", "date"],
	]
	var valueData = [];

	return {
		title: 'Text Field Properties',
		minWidth: 400,
		minHeight: 200,
		getModel: function (a) {
			a = a.getSelection().getSelectedElement();
			return !a ||
				"input" != a.getName() ||
				(!g[a.getAttribute("type")] && a.getAttribute("type"))
				? null
				: a;
		},
		onShow: function () {
			var a = this.getModel(this.getParentEditor());
			a && this.setupContent(a);
		},
		onOk: function () {
		var a = this.getParentEditor(),
			b = this.getModel(a),
			c = this.getMode(a) == CKEDITOR.dialog.CREATION_MODE;
		c &&
			((b = a.document.createElement("input")),
			b.setAttribute("type", "text"));
		b = { element: b };
		c && a.insertElement(b.element);
		this.commitContent(b);
		c || a.getSelection().selectElement(b.element);
		var data = this;
		this.getParentEditor().insertText("{{"+data.getValueOf('tab1', 'placeholder')+"}}\n");
		console.log(data.getValueOf('tab1', '_cke_saved_name'))
		},
		onLoad:  function () {
			
			this.foreach(function (a) {
				a.getValue && (a.setup || (a.setup = f), a.commit || (a.commit = e));
			});
		},
		contents: [
			{
				id: 'tab1',
				label: 'First Tab',
				title: 'First Tab',
				elements: [
					{
						type: "hbox",
						widths: ["50%", "50%"],
						children: [
							{
								id: '_cke_saved_name',
								type: 'text',
								label: 'Name',
								setup: function (a) {
									this.setValue(
										a.data("cke-saved-name") || a.getAttribute("name") || ""
									);
								},
								commit: function (a) {
								a = a.element;
								this.getValue()
									? a.data("cke-saved-name", this.getValue())
									: (a.data("cke-saved-name", !1), a.removeAttribute("name"));
								},
							},
							{
								id: 'value',
								type: 'text',
								label: 'Value',
								commit: function (a) {
									if (CKEDITOR.env.ie && !this.getValue()) {
										var d = a.element,
										c = new CKEDITOR.dom.element("input", b.document);
										d.copyAttributes(c, { value: 1 });
										c.replace(d);
										a.element = c;
									} else e.call(this, a);
								},
							},
						]
					},
					{
						type: "hbox",
						widths: ["50%", "50%"],
						children: [
							{
								id: 'size',
								type: 'text',
								label: 'Character Width',
								validate: CKEDITOR.dialog.validate.integer(
									b.lang.common.validateNumberFailed
								),
							},
							{
								id: 'maxLength',
								type: 'text',
								label: 'Maximum Character',
								validate: CKEDITOR.dialog.validate.integer(
									b.lang.common.validateNumberFailed
								),
							},
						]
					},
					{
						id: 'type',
						type: 'select',
						label: 'Type',
						default: 'text',
						items: types,
						setup: function (a) {
							this.setValue(a.getAttribute("type"));
						},
						commit: function (a) {
						var d = a.element;
						if (CKEDITOR.env.ie) {
							var c = d.getAttribute("type"),
							e = this.getValue();
							c != e &&
							((c = CKEDITOR.dom.element.createFromHtml(
								'\x3cinput type\x3d"' + e + '"\x3e\x3c/input\x3e',
								b.document
							)),
							d.copyAttributes(c, { type: 1 }),
							c.replace(d),
							(a.element = c));
						} else d.setAttribute("type", this.getValue());
						}
					},
					{
						id: "placeholder",
						type: "select",
						label: 'Data Field',
						items: valueData,
						onLoad: async function () {
                            await axios.get('https://sandbox.digitaldoc.pullstream.com/api/datafields',{
								headers: {
								'Authorization': 'Bearer 7391f62dcbde414fba30d64ded65930e'
								}
							})
							.then((response) => {
								var datafield = response.data['results']
								datafield.forEach((value) => {
									valueData.push([value['name']])
								})
							});
							valueData.forEach((item) => {
								this.add(item);
							});
                        },
					},
					{
						type: 'checkbox',
						id: 'myCheckbox',
						label: 'Required',
						'default': false,
						setup: CKEDITOR.plugins.forms._setupRequiredAttribute,
						commit: function (a) {
						a = a.element;
						this.getValue()
							? a.setAttribute("required", "required")
							: a.removeAttribute("required");
						},
					},

				]
			}
		]
	};
} 
);
